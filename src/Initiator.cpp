#include "Initiator.h"

#define BOID_SIZE 1
#define FLOWFIELD_RES 10

ofPath p;
ofVec2f lastPos, a, b;
int sh, sw;

//--------------------------------------------------------------
void Initiator::setup(){
	ofSetFrameRate(30);
	ofBackground(10, 10, 13);
	//ofBackground(0);	
	ofSetBackgroundAuto(false);
	ofEnableAlphaBlending();

	postFx.init(ofGetWindowWidth(), ofGetWindowHeight());
	postFx.createPass<BloomPass>();
	postfxToggle = false;

	debugMode = false;
	trailEffect = false;

	grad.loadImage("grad.png");
	//boid = new Boid(ofRandom(0, ofGetWidth()), ofRandom(0, ofGetHeight()), 1, BOID_SIZE);

	flowfield.setup(FLOWFIELD_RES);
	flowfield.init();

	for(int i = 0; i < 10; i++){
		Boid b(ofRandom(0, ofGetWidth()), ofRandom(0, ofGetHeight()), 1, BOID_SIZE, &flowfield);
		b.color = grad.getColor(ofRandom(0,1024), 5);
		boids.push_back(b);
	}

	sw = ofGetWidth();
	sh = ofGetHeight();

	a = ofVec2f(0, sh - 10);
	b = ofVec2f(sw, sh - 10);
}

//--------------------------------------------------------------
void Initiator::update(){
	//boid->arrive(ofVec2f(ofGetMouseX(), ofGetMouseY()));
	unsigned int len = boids.size();
	for (int i = 0; i < len; i++) {
		boids[i].follow();
		boids[i].update();
		boids[i].checkEdge();
	}

	sw = ofGetWidth();
	sh = ofGetHeight();

	ofSetBackgroundAuto(!trailEffect);
	ofSetWindowTitle(ofToString(ofGetFrameRate()));
}

//--------------------------------------------------------------
void Initiator::draw(){
	if(trailEffect){		
		ofSetColor(0,10);
		ofRect(0, 0, sw, sh);
	}

	if(postfxToggle){
		postFx.begin();
		postFx.setFlip(false);
	}

	unsigned int len = boids.size();
	for (int i = 0; i < len; i++) {
		//ofColor col = grad.getColor(ofRandom(0,1024), 5);
		boids[i].draw();
	
		/*ofVec2f p = scalerProjection(boids[i].position, a, b);
		ofLine(0, sh - 10, sw, sh - 10);
		ofLine(boids[i].position.x, boids[i].position.y, p.x, p.y);
		ofCircle(boids[i].position.x, boids[i].position.y, 10);*/
	}

	if(debugMode) flowfield.draw();
	if(postfxToggle) postFx.end();
}

ofVec2f Initiator::scalerProjection(ofVec2f target, ofVec2f  a, ofVec2f  b) {
	ofVec2f dirT = target - a;
	ofVec2f dirO = b - a;
	dirO.normalize();

	float theta = dirT.dot(dirO);
	
	return (dirO * theta) + a;
}

//--------------------------------------------------------------
void Initiator::keyPressed(int key){

}

//--------------------------------------------------------------
void Initiator::keyReleased(int key){
	/*if(key == 'd'){
		cout << "debug mode: " << debugMode << endl;
		debugMode = !debugMode;
	}*/

	switch(key){
		case 'd':
			cout << "debug mode: " << debugMode << endl;
			debugMode = !debugMode;
			break;
		case 'g':
			flowfield.setup(FLOWFIELD_RES);
			flowfield.init();
			break;
		case 'p':
			postfxToggle = !postfxToggle;
			trailEffect = postfxToggle ? false : trailEffect;
			break;
		case 't':
			trailEffect = !trailEffect;
			postfxToggle = trailEffect ? false : postfxToggle;
			break;
	}
}

//--------------------------------------------------------------
void Initiator::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void Initiator::mouseDragged(int x, int y, int button){
	Boid b(x, y, 1, BOID_SIZE, &flowfield);
	b.color = grad.getColor(ofRandom(0,1024), 5);
	boids.push_back(b);
}

//--------------------------------------------------------------
void Initiator::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void Initiator::mouseReleased(int x, int y, int button){
	cout << boids.size() << endl;
}

//--------------------------------------------------------------
void Initiator::windowResized(int w, int h){
	flowfield.setup(FLOWFIELD_RES);
	flowfield.init();
}

//--------------------------------------------------------------
void Initiator::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void Initiator::dragEvent(ofDragInfo dragInfo){ 

}
