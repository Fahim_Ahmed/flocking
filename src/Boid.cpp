#include "Boid.h"

Boid::Boid() {
}

Boid::Boid(float x, float y, float mass, float size) {
	setup(x, y, mass, size);
}

Boid::Boid(float x, float y, float mass, float size, FlowField *flowfield) {
	setup(x, y, mass, size);
	this->flowfield = flowfield;
}

//--------------------------------------------------------------
void Boid::setup(float x, float y, float mass, float size){
	this->position.x = x;
    this->position.y = y;
    this->mass = mass;
	this->size = size;

	this->lastPos.x = x;
	this->lastPos.y = y;

	maxSpeed = 5;
	maxForce = 0.1;
}

//--------------------------------------------------------------
void Boid::update(){
	velocity += acceration;
	velocity.limit(maxSpeed);

	position += velocity;
	acceration *= 0;
}

void Boid::applyForce(ofVec2f force) {
    acceration = acceration + (force/mass);
}

void Boid::follow(){
	ofVec2f desired = flowfield->lookup(position);
	desired.normalize();
	desired *= maxSpeed;

	ofVec2f steer = desired - velocity;
	steer.limit(maxForce);

	applyForce(steer);
}

void Boid::seek(ofVec2f target) {
	ofVec2f desired = target - position;
	desired.normalize();
	desired *= maxSpeed;

	ofVec2f steer = desired - velocity;
	steer.limit(maxForce);

	applyForce(steer);
}

void Boid::arrive(ofVec2f target) {
	ofVec2f desired = target - position;
	float distance = desired.length();

	desired.normalize();

	if(distance < 100) {
		desired *= ofMap(distance, 0, 100, 0, maxSpeed, false);
	}else {
		desired *= maxSpeed;
	}

	ofVec2f steer = desired - velocity;
	steer.limit(maxForce);

	applyForce(steer);
}

//--------------------------------------------------------------
void Boid::draw(ofColor col){
	// Draw a triangle rotated in the direction of velocity
	float angle = (float)atan2(-velocity.y, velocity.x);
    float theta =  -1.0*angle;
	float heading2D = ofRadToDeg(theta);

	ofSetColor(col);

	ofPushMatrix();
	ofTranslate(position.x, position.y);
	ofRotateZ(heading2D);
	
	//ofSetPolyMode(OF_POLY_WINDING_NONZERO);
	ofBeginShape();
	ofVertex(0, 0);
	ofVertex(- 10 * size, 3 * size);
	ofVertex(- 7.5 * size, 0);
	ofVertex(- 10 * size, - 3 * size);	
	ofEndShape(true);

	ofPopMatrix();

	/*p.setStrokeWidth(5);
	p.setStrokeColor(255);
	p.setFilled(false);

	p.clear();
	p.moveTo(position.x, position.y);
	p.lineTo(lastPos.x, lastPos.y);
	p.draw();*/

	lastPos.x = position.x;
	lastPos.y = position.y;
}

//--------------------------------------------------------------
void Boid::draw(){
	// Draw a triangle rotated in the direction of velocity
	float angle = (float)atan2(-velocity.y, velocity.x);
    float theta =  -1.0*angle;
	float heading2D = ofRadToDeg(theta);

	//ofEnableBlendMode(OF_BLENDMODE_ADD);

	ofSetColor(color);

	ofPushMatrix();
	ofTranslate(position.x, position.y);
	ofRotateZ(heading2D);
	
	//ofSetPolyMode(OF_POLY_WINDING_NONZERO);
	ofBeginShape();
	ofVertex(0, 0);
	ofVertex(- 10 * size, 3 * size);
	ofVertex(- 7.5 * size, 0);
	ofVertex(- 10 * size, - 3 * size);	
	ofEndShape(true);

	ofPopMatrix();
	//ofDisableBlendMode();
	/*p.setStrokeWidth(5);
	p.setStrokeColor(255);
	p.setFilled(false);

	p.clear();
	p.moveTo(position.x, position.y);
	p.lineTo(lastPos.x, lastPos.y);
	p.draw();*/

	lastPos.x = position.x;
	lastPos.y = position.y;
}

void Boid::edgeBounce() {
    if (position.x > ofGetWindowWidth()) {
      velocity.x *= -1;
    }
    else if (position.x < 0) {
      velocity.x *= -1;
    }

    if (position.y > ofGetWindowHeight()) {
      velocity.y *= -1;
    }
    else if (position.y < 0) {
      velocity.y *= -1;
    }
}

void Boid::checkEdge() {
    if (position.x > ofGetWindowWidth()) {
      position.x = 0;
	  lastPos.x = 0;
    }
    else if (position.x < 0) {
      position.x = ofGetWindowWidth();
      lastPos.x = ofGetWindowWidth();
    }

    if (position.y > ofGetWindowHeight()) {
      position.y = 0;
      lastPos.y = 0;
    }
    else if (position.y < 0) {
      position.y = ofGetWindowHeight();
      lastPos.y = ofGetWindowHeight();
    }
}
