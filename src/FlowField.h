#include "ofMain.h"

class FlowField {

private:
	vector<vector<ofVec2f>> field;
	int rows, cols, resolution;
	float anti_res;


public:

	FlowField(){}

	void setup(int res = 30){
		this->resolution = res;

		rows = ofGetHeight() / resolution;
		cols = ofGetWidth() / resolution;

		field.resize(cols);
		for (int i = 0; i < cols; i++) {
			field[i].resize(rows);
		}

		init();
	}

	void init() {
		float xSeed = ofRandom(0,1);
		float ySeed = ofRandom(0,1);
		float yOff;

		for (int i = 0; i < cols; i++) {
			float yOff = ySeed;
			for (int j = 0; j < rows; j++) {
				float theta = ofMap(ofNoise(xSeed, yOff), 0, 1, 0, -TWO_PI);
				field[i][j].set(cos(theta), sin(theta));

				yOff += 0.03;
			}

			xSeed += 0.03;
		}

		anti_res = 1 / (float) resolution;
	}

	 void draw() {
      for (int i = 0; i < cols; i++) {
        for (int j = 0; j < rows; j++) {
          drawVector(field[i][j], i*resolution, j*resolution, resolution-2);
        }
      }
    }

    void drawVector(const ofVec2f & v, float x, float y, float scayl) {
      ofPushMatrix();
      float arrowsize = 4;
	  ofTranslate(x + resolution, y + resolution);

      ofRotate(ofVec2f(1,0).angle(v));
      float len = v.length()*scayl;

	  ofLine(0,0,len,0);
      /*ofLine(len,0,len-arrowsize,+arrowsize/2);
      ofLine(len,0,len-arrowsize,-arrowsize/2);*/
      ofPopMatrix();
    }

	ofVec2f lookup(ofVec2f loc) {
		int col = ofClamp(loc.x * anti_res, 0, cols - 1);
		int row=  ofClamp(loc.y * anti_res, 0, rows- 1);

		return field[col][row];
	}
};