#pragma once

#include "ofMain.h"
#include "Boid.h"
#include "ofxPostProcessing.h"

class Initiator : public ofBaseApp{
	private:
		Boid *boid;
		ofImage grad;
		FlowField flowfield;
		bool debugMode, postfxToggle, trailEffect;
		vector<Boid> boids;
		ofxPostProcessing postFx;

	public:
		void setup();
		void update();
		void draw();
		ofVec2f scalerProjection(ofVec2f target, ofVec2f  a, ofVec2f  b);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
