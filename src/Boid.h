#pragma once

#include "ofMain.h"
#include "FlowField.h"

class Boid : public ofBaseApp{
	private:
		ofVec2f		lastPos;
		ofPath		p;
		FlowField	*flowfield;		

	public:
		ofVec2f		position;
		ofVec2f		velocity;
        ofVec2f		acceration;
		float		mass;
		float		size;
		float		maxSpeed;
		float		maxForce;
		ofColor		color;		

		Boid();
		Boid(float x, float y, float mass, float size);
		Boid(float x, float y, float mass, float size, FlowField *flowfield);

		void setup(float x, float y, float mass, float size);
		void update();
		void draw();
		void draw(ofColor col);
		void applyForce(ofVec2f force);		

		void seek(ofVec2f target);
		void arrive(ofVec2f target);
		void follow();

		void checkEdge();
        void edgeBounce();
};
